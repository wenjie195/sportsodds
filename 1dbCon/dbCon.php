<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_admintele");//for localhost
    // $conn = new mysqli("localhost", "ichibang_tmuser", "Ci6FcI4Qsp3h","ichibang_telemarketing"); //for live server
    // $conn = new mysqli("localhost", "dxforext_teleuser", "HUwFhA2c8Opu","dxforext_telemarketing"); //for live server
    // $conn = new mysqli("localhost", "ichibang_teleusr", "MduzT5j6em5A","ichibang_newtelemarket"); //for live server

    // $conn = new mysqli("localhost", "ichibang_tele2", "oyLaYRvv62rY","ichibang_tele"); //for live server


    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
