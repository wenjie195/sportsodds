<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Account Creation | adminTele" />
    <title>Account Creation | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">
	<h1 class="h1-title">Account Creation</h1> 
        <form   action="utilities/addNewRefereeFunction.php" method="POST">
        <div class="input50-div">
			<p class="input-title-p">Name</p>
            <input class="clean tele-input" type="text" placeholder="Name" id="register_fullname" name="register_fullname" required>        
        </div> 
        <div class="input50-div second-input50">
			<p class="input-title-p">Email</p>
            <input class="clean tele-input"  type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>        
        </div> 
        <div class="clear"></div>
        <div class="input50-div">
        	<p class="input-title-p">Address</p>
            <input class="clean tele-input"  type="text" placeholder="Address" id="register_address" name="register_address" required>  
        </div>
        <div class="input50-div second-input50">
        	<p class="input-title-p">Contact</p>
            <input class="clean tele-input"  type="text" placeholder="Contact" id="register_phone" name="register_phone" required>  
        </div>         
        <div class="clear"></div>
        <div class="input50-div">
        	<p class="input-title-p">Password</p>
            <input class="clean tele-input"  type="password" placeholder="Password" id="register_password" name="register_password" required>  
        </div>
        <div class="input50-div second-input50">
        	<p class="input-title-p">Retype Password</p>
            <input class="clean tele-input"  type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>  
        </div>
		<div class="clear"></div>
        <div class="input50-div">
        	<p class="input-title-p">Account Type</p>
                        <select class="clean tele-input" type="text" id="register_account_type" name="register_account_type" required>
                        <option value="" name=" ">PLEASE SELECT ACCOUNT TYPE</option>
                        <option value="0" name="0">Admin</option>
                        <option value="1" name="1">Telemarketer</option>
                        <option value="3" name="3">Company</option>
                        <!-- <option value="Admin" name="Admin">Admin</option>
                        <option value="Telemarketer" name="Telemarketer">Telemarketer</option> -->
                        </select> 
        </div>

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Create Account</button>
        </form>

       
</div>
<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<?php include 'js.php'; ?>
</body>
</html>