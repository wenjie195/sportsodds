<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

// $statusDetails = getStatus($conn);
// $reasonDetails = getReason($conn);

$companyDetails = getCompanySelection($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Company | adminTele" />
    <title>Company | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">Company</h1>

    <div class="clear"></div>

    <!-- <div class="width100 extra-m-btm">
    	<button class="clean red-btn margin-top30 fix300-btn add-status-btn text-center open-status">Add Status/Reason</button>
    </div> -->

    <div class="width100 extra-m-btm">
    	<button class="clean red-btn margin-top30 fix300-btn add-status-btn text-center open-company">Add Company</button>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>COMPANY NAME</th>
                            <th>DATE CREATED</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($companyDetails)
                        {   
                            for($cnt = 0;$cnt < count($companyDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $companyDetails[$cnt]->getCompanyName();?></td>
                                <td><?php echo $companyDetails[$cnt]->getDateCreated();?></td>

                                <td>
                                    <form action="adminEditCompany.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="company_name" value="<?php echo $companyDetails[$cnt]->getCompanyName();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>
</div>
<style>
.company-li{
	color:#bf1b37;
	background-color:white;}
.company-li .hover1a{
	display:none;}
.company-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>