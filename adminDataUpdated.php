<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');

$soundfile = "noti.mp3";

//additional hrs or mins
// $cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-1 minutes',strtotime($time)));
$cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-1 seconds',strtotime($time)));

// $updatedCustomerDetails = getCustomerDetails($conn," WHERE no_of_call > 0 AND no_of_call <=2 AND status != 'Good' ");
// $goodCustomerDetails = getCustomerDetails($conn," WHERE status = 'Good' ");
// $badCustomerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ");

$updatedCustomerDetails = getCustomerDetails($conn," WHERE no_of_call > 0 AND no_of_call <=2 AND status != 'Good' AND date_updated >= '".$cenvertedTimeMin."'");
$goodCustomerDetails = getCustomerDetails($conn," WHERE status = 'Good' AND date_updated >= '".$cenvertedTimeMin."'");
$badCustomerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' AND date_updated >= '".$cenvertedTimeMin."'");

$conn->close();

?>

    <?php
        if($updatedCustomerDetails)
        {   
            $totalUpdatedCustomerDetails = count($updatedCustomerDetails);
        }
        else
        {   
            $totalUpdatedCustomerDetails = 0;   
    }
    ?>

    <?php
        if($goodCustomerDetails)
        {   
            $totalGoodCustomerDetails = count($goodCustomerDetails);
            echo "<embed src =\"$soundfile\" hidden=\"true\" autostart=\"true\"></embed>";
        }
        else
        {   
            $totalGoodCustomerDetails = 0;   
        }
    ?>

    <?php
        if($badCustomerDetails)
        {   
            $totalBadCustomerDetails = count($badCustomerDetails);
        }
        else
        {   
            $totalBadCustomerDetails = 0;   
        }
    ?>

    <!-- <div class="right-data-div">
        5 Data Updated<br>
        10 <b class="green-text">Good</b> updated<br>
        8 <b class="pink-text">Bad</b> updated
    </div> -->

    <div class="right-data-div">
        <?php echo $totalUpdatedCustomerDetails; ?> Data Updated<br>
        <?php echo $totalGoodCustomerDetails; ?> <b class="green-text">Good</b> Updated<br>
        <?php echo $totalBadCustomerDetails; ?> <b class="pink-text">Bad</b> Updated
    </div>