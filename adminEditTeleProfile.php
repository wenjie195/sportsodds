<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$userRows = getUser($conn," WHERE user_type =1 ");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Telemarketer Profile | adminTele" />
    <title>Edit Telemarketer Profile | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <!-- <h1 class="h1-title">Edit Company</h1> -->

    <h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Edit Telemarketer Profile
        </a>
    </h1>

    <div class="clear"></div>

    <?php
    if(isset($_POST['tele_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['tele_uid']),"s");
    ?>

    <form action="utilities/adminEditTeleProfileFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Name</p>
            <input class="clean tele-input" type="text" placeholder="Name" value="<?php echo $userDetails[0]->getUsername();?>" id="update_username" name="update_username" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Email</p>
            <input class="clean tele-input"  type="email" placeholder="Email" value="<?php echo $userDetails[0]->getEmail();?>" id="update_email" name="update_email" required>        
        </div>

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Address</p>
            <input class="clean tele-input"  type="text" placeholder="Address" value="<?php echo $userDetails[0]->getAddress();?>" id="update_address" name="update_address" required>  
        </div>

        <div class="input50-div second-input50">
            <p class="input-title-p">Contact</p>
            <input class="clean tele-input"  type="text" placeholder="Contact" value="<?php echo $userDetails[0]->getPhoneNo();?>" id="update_phone" name="update_phone" required>  
        </div>         

        <div class="clear"></div>

        <input class="clean tele-input"  type="hidden" value="<?php echo $userDetails[0]->getUid();?>" id="tele_uid" name="tele_uid" readonly> 

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>
        
    </form>

    <?php
    }
    ?>

</div>

<!-- <style>
.company-li{
	color:#bf1b37;
	background-color:white;}
.company-li .hover1a{
	display:none;}
.company-li .hover1b{
	display:block;}
</style> -->

<style>
.telemarketer-li{
	color:#bf1b37;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

<script>
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>