<div class="side-bar red-bg">
	<div class="width100 text-center">
    	<img src="img/logo.png" class="logo-img" alt="logo" title="logo">
    </div>
	<ul class="sidebar-ul">
    	<a href="adminDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>
    	<a href="addReferee.php">
        	<li class="sidebar-li account-li hover1">
            	<img src="img/create-account1.png" class="hover1a" alt="Account Creation" title="Account Creation">
                <img src="img/create-account2.png" class="hover1b" alt="Account Creation" title="Account Creation">
                 <p>Account Creation</p>
            </li>
        </a>
        <a href="viewTeleList.php">
        	<li class="sidebar-li telemarketer-li hover1">
            	<img src="img/telemarketer1.png" class="hover1a" alt="Telemarketer" title="Telemarketer">
                <img src="img/telemarketer2.png" class="hover1b" alt="Telemarketer" title="Telemarketer">
                 <p>Telemarketer</p>
            </li>
        </a>

        <!-- <a href="teleKPIList.php">
        	<li class="sidebar-li telemarketerkpi-li hover1">
            	<img src="img/telemarketer1.png" class="hover1a" alt="Telemarketer" title="Telemarketer">
                <img src="img/telemarketer2.png" class="hover1b" alt="Telemarketer" title="Telemarketer">
                 <p>KPI</p>
            </li>
        </a> -->

        <a href="checkLog.php">
        	<li class="sidebar-li customer-li hover1">
            	<img src="img/customer1.png" class="hover1a" alt="Customer" title="Customer">
                <img src="img/customer2.png" class="hover1b" alt="Customer" title="Customer">            
            	 <p>Customer</p>
            </li>
        </a>

        <!-- <a href="checkLogUpdated2.php">
        	<li class="sidebar-li second-level-li hover1">
                <img src="img/filter2.png" class="hover1a" alt="Customer" title="Customer">
                <img src="img/filter.png" class="hover1b" alt="Customer" title="Customer">   
                 <p>2nd Level Filter</p>
            </li>
        </a> -->

        <a href="checkLogFilterLevelGood.php">
        	<li class="sidebar-li second-level-li hover1">
                <img src="img/filter2.png" class="hover1a" alt="Customer" title="Customer">
                <img src="img/filter.png" class="hover1b" alt="Customer" title="Customer">   
                 <p>2nd Filter (G / NTC)</p>
            </li>
        </a>
        
        <a href="checkLogRecall.php">
        	<li class="sidebar-li customer-recall-li hover1">
            	<img src="img/customer-recall2.png" class="hover1a" alt="Customer" title="Customer">
                <img src="img/customer-recall.png" class="hover1b" alt="Customer" title="Customer">            
            	 <p>Customer (Recall)</p>
            </li>
        </a>

        <a href="adminStatusReason.php">
        	<li class="sidebar-li statusreason-li hover1">
            	<img src="img/status-reason.png" class="hover1a" alt="Status Reason" title="Status Reason">
                <img src="img/status-reason2.png" class="hover1b" alt="Status Reason" title="Status Reason">            
            	 <p>Status Reason</p>
            </li>
        </a>
        <a href="adminCompany.php">
        	<li class="sidebar-li company-li hover1">
            	<img src="img/company2.png" class="hover1a" alt="Company" title="Company">
                <img src="img/company.png" class="hover1b" alt="Company" title="Company">            
            	 <p>Company List</p>
            </li>
        </a>
        <a href="uploadExcel.php">
        	<li class="sidebar-li import-li hover1">
            	<img src="img/import-data1.png" class="hover1a" alt="Import Data" title="Import Data">
                <img src="img/import-data2.png" class="hover1b" alt="Import Data" title="Import Data">            
             	 <p>Import Data</p>
            </li>
        </a>
        <a href="emer.php">
        	<!-- <li class="sidebar-li hover1"> -->
            <li class="sidebar-li emer-li hover1">
            	<img src="img/EMGC.png" class="hover1a" alt="EMGC" title="EMGC">
                <img src="img/EMGC2.png" class="hover1b" alt="EMGC" title="EMGC">  
            	 <p>EMGC</p>
            </li>
       </a>
        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <img src="img/logo.png" class="mobile-logo" alt="Logo" title="Logo">
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <a href="adminDashboard.php">
                    <li class="sidebar-li dashboard-li hover1">
                        <img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Dashboard</p>
                    </li>
                </a>
                <a href="addReferee.php">
                    <li class="sidebar-li account-li hover1">
                        <img src="img/create-account1.png" class="hover1a" alt="Account Creation" title="Account Creation">
                        <img src="img/create-account2.png" class="hover1b" alt="Account Creation" title="Account Creation">
                         <p>Account Creation</p>
                    </li>
                </a>
                <a href="viewTeleList.php">
                    <li class="sidebar-li telemarketer-li hover1">
                        <img src="img/telemarketer1.png" class="hover1a" alt="Telemarketer" title="Telemarketer">
                        <img src="img/telemarketer2.png" class="hover1b" alt="Telemarketer" title="Telemarketer">
                         <p>Telemarketer</p>
                    </li>
                </a>
                <a href="checkLog.php">
                    <li class="sidebar-li customer-li hover1">
                        <img src="img/customer1.png" class="hover1a" alt="Customer" title="Customer">
                        <img src="img/customer2.png" class="hover1b" alt="Customer" title="Customer">            
                         <p>Customer</p>
                    </li>
                </a>
                <a href="adminStatusReason.php">
                    <li class="sidebar-li statusreason-li hover1">
                        <img src="img/status-reason.png" class="hover1a" alt="Status Reason" title="Status Reason">
                        <img src="img/status-reason2.png" class="hover1b" alt="Status Reason" title="Status Reason">            
                         <p>Status Reason</p>
                    </li>
                </a>                
                
                
                
                
                 <a href="adminCompany.php">
                    <li class="sidebar-li  company-li hover1">
                        <img src="img/company2.png" class="hover1a" alt="Company" title="Company">
                        <img src="img/company.png" class="hover1b" alt="Company" title="Company">            
                         <p>Company List</p>
                    </li>
                </a>               
                
                
                <a href="uploadExcel.php">
                    <li class="sidebar-li import-li hover1">
                        <img src="img/import-data1.png" class="hover1a" alt="Import Data" title="Import Data">
                        <img src="img/import-data2.png" class="hover1b" alt="Import Data" title="Import Data">            
                         <p>Import Data</p>
                    </li>
                </a>
                <a href="emer.php">
                    <li class="sidebar-li emer-li hover1">
                        <img src="img/EMGC.png" class="hover1a" alt="EMGC" title="EMGC">
                        <img src="img/EMGC2.png" class="hover1b" alt="EMGC" title="EMGC">          
                         <p>EMGC</p>
                    </li>
                </a>                
                
                
                <a href="logout.php">
                    <li class="sidebar-li hover1">
                        <img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                        <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
                         <p>Logout</p>
                    </li>
               </a>
            </ul>
    </div>
</header>