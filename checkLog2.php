<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
// require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';


$uid = $_SESSION['uid'];

$conn = connDB();

// $userCnt = getUser($conn, "WHERE user_type = 1");
$userCnt = getCustomerDetails($conn," WHERE no_of_call = 0 ");

$userCount = count($userCnt);

if (isset($_GET['page']))
{
  $page = $_GET['page'];  // get page from link ?page=5
  $limitMax = 200; // 50 user per page
  $startColumn = ($page - 1) * $limitMax; // start from? page = 2.. (2-1) * 50 = 50, start 50, LIMIT 50,50 start from 50 to next 50 user.
  // $userDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn,$limitMax ");
  $customerDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn,$limitMax ");
}
else 
{
  $limitMax = 200;
  $startColumn = 0;
  // $userDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn,$limitMax ");
  $customerDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn,$limitMax ");
}

// $startColumn += 1; // for table no column
// $pagination = $userCount / $limitMax; // no of page

$startColumn += 0; // for table no column
$pagination = ($userCount / $limitMax) + 1 ; // no of page

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
<meta property="og:title" content="Customer Details | adminTele" />
<!-- <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'"> -->
<title>Customer Details | adminTele</title>
<!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->

<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"> </script>'; ?> -->
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title customer-h1">Customer</h1>

      <div id="divDataUpdated">

      </div>

    <div class="clear"></div>

  <h4 class="tab-h2">Customer Details | <a href="checkLogUpdated.php" class="red-link">Updated</a> | <a href="checkLogGood.php" class="red-link">Good</a> | <a href="checkLogBlack.php" class="red-link">Blacklist</a> </h4>

  <div class="clear"></div>

  <div class="big-four-input-container">
      <div class="four-input-div first-four-div">
        <p class="input-top-p">Phone Number</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Phone Number" class="tele-four-input tele-input clean">
      </div>
    
      <form method='post' action=''> 
        <?php
        if(isset($_POST['reset']))
        {
        ?>
        <div class="four-input-div left-four-input">
            <p class="input-top-p">From Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='fromDate'>
        </div>
        <div class="four-input-div right-four-input">
            <p class="input-top-p">To Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='endDate' >
        </div>    
        <?php
        }
        else
        {
        ?>
        <div class="four-input-div left-four-input">
            <p class="input-top-p">From Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='fromDate' value='<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>'>
        </div>
        <div class="four-input-div right-four-input">
            <p class="input-top-p">To Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='endDate' value='<?php if(isset($_POST['endDate'])) echo $_POST['endDate']; ?>'>
        </div>
        <?php
        }
        ?>
        <div class="four-input-div last-four-div">
            <input type='submit' name='but_search' value='Search' class="submit-btn clean">
            <input type='submit' name='reset' value='Reset' class="submit-btn clean right-submit-btn">
        </div>
      </form>
	</div>
    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>STATUS</th>
                            <th>REMARK</th>
                            <th>LAST UPDATED</th>
                            <th>REVIEW</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        // Date filter
                        if(isset($_POST['but_search']))
                        {
                          $fromDate = $_POST['fromDate'];
                          $endDate = $_POST['endDate'];
                          if(!empty($fromDate) && !empty($endDate))
                          {
                            //$customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND last_updated between '".$fromDate."' and '".$endDate."'");
                            $customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND last_updated between '".$fromDate."' and '".$endDate."' LIMIT $startColumn,$limitMax ");
                          }
                        }
                        elseif (isset($_POST['reset']))
                        {
                          $customerDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn, $limitMax ");
                        }
                        else
                        {
                          $customerDetails = getCustomerDetails($conn, "WHERE no_of_call = 0 LIMIT $startColumn, $limitMax ");
                        }

                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <!-- <td><?php echo ($cnt+1)?></td> -->
                                <td><?php echo ($startColumn++)?></td>
                                <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                                <td><?php echo $customerDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $customerDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $customerDetails[$cnt]->getRemark();?></td>

                                <td><?php echo date("d-m-Y",strtotime($customerDetails[$cnt]->getLastUpdated()));?></td>
                                
                                <td>
                                    <form action="reviewCustomerDetails.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="customer_name" value="<?php echo $customerDetails[$cnt]->getPhone();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        $conn->close();
                        ?>
                    </tbody>

                </table>
            </div>
    </div>

    <div class="pagination pagination-pop">
      <?php for ($i=1; $i < $pagination ; $i++)
      {
      ?>
        <?php if (isset($_GET['page']) && $_GET['page'] == $i) 
      {
      ?>
        <a class="active pagination-pop-a pagination-pop-a-active" href="<?php echo "checkLog2.php?page=".$i ?>"><?php echo $i ?></a>
      <?php
      }
      else
      {
      ?>
        <a class="pagination-pop-a" href="<?php echo "checkLog2.php?page=".$i ?>"><?php echo $i ?></a>
      <?php
      }
      ?>
      <?php
      }
      ?>
    </div>
        
</div>

<style>
.customer-li{
	color:#bf1b37;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#toDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

<script type='text/javascript'>
  $(document).ready(function()
  {
    $('.dateFilter').datepicker(
    {
      dateFormat: "yy-mm-dd"
    });
  });
</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#divDataUpdated").load("adminDataUpdated.php");
setInterval(function()
{
    $("#divDataUpdated").load("adminDataUpdated.php");
}, 2000);
});
</script>

</body>
</html>