<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
// require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$compRows = getUser($conn," WHERE user_type = 1 ");
$compDetails = $compRows;

// $customerDetails = getCustomerDetails($conn, "WHERE action = 'Done' AND filter = 'Pending' ORDER BY last_updated DESC ");
// $customerDetails = getCustomerDetails($conn, "WHERE type = 'No Take Call' AND reason = 'No Take Call' AND no_of_call = '1' ORDER BY last_updated DESC ");
$customerDetails = getCustomerDetails($conn, "WHERE type = 'No Take Call' AND reason = 'No Take Call' AND no_of_call = '1' ORDER BY last_updated DESC LIMIT 4000");
// $customerDetails = getCustomerDetails($conn, "WHERE type = 'No Take Call' AND reason = 'No Take Call' AND no_of_call = '1' ORDER BY last_updated DESC LIMIT 4000");

// $customerDetails = getCustomerDetails($conn, "WHERE action != '' AND filter != 'Update' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
<meta property="og:title" content="Customer Details | adminTele" />
<!-- <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'"> -->
<title>Customer Details | adminTele</title>
<!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->

<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"> </script>'; ?> -->
<?php include 'adminSidebar.php'; ?>
<!-- -->
<div class="next-to-sidebar">

    <h1 class="h1-title customer-h1">2nd Level Filter (No Take Call)</h1>

      <!-- <div id="divDataUpdated">

      </div> -->

    <div class="clear"></div>

    <h4 class="tab-h2"><a href="checkLogFilterLevelGood.php" class="red-link">Good Customer </a> | No Take Call </h4>

    <!-- <form method="POST" action="utilities/adminSecondFilter.php"> -->
    <form method="POST" action="utilities/adminSecondFilterNTC.php">

      <?php
      if($compDetails)
      {
        for ($cntA=0; $cntA <count($compDetails) ; $cntA++)
        {
        ?>
            <button  class="clean  tele-btn" type="submit" id = "submit" name="new_tele" value="<?php echo $compDetails[$cntA]->getUsername(); ?>">
              <?php echo $compDetails[$cntA]->getUsername(); ?>
            </button>

        <?php
        }
      }
      ?>

    <div class="clear"></div>

    <div class="width100 shipping-div2 margin-top20">
            <div class="overflow-scroll-div">
                <table class="shipping-table width100" id="myTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>STATUS</th>
                            <!-- <th>REASON</th> -->
                            <!-- <th>COMPANY</th> -->
                            <th>REMARK</th>
                            <th>UPDATED BY</th>
                            <th>LAST UPDATED</th>
                            <th>REVIEW</th>
                            <!-- <th>ACTION</th> -->
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        if($customerDetails)
                        {
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td>
                                  <input name="arr[]" type="checkbox" value="<?php echo $customerDetails[$cnt]->getPhone();?>">
                                </td>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                                <td><?php echo $customerDetails[$cnt]->getType();?></td>
                                <!-- <td><?php //echo $customerDetails[$cnt]->getReason();?></td> -->
                                <!-- <td><?php //echo $customerDetails[$cnt]->getCompanyName();?></td> -->
                                <td><?php echo $customerDetails[$cnt]->getRemark();?></td>
                                <td><?php echo $customerDetails[$cnt]->getTeleName();?></td>
                                <td><?php echo date("d-m-Y",strtotime($customerDetails[$cnt]->getLastUpdated()));?></td>

                                <td>
                                    <form action="reviewCustomerDetails.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="customer_name" value="<?php echo $customerDetails[$cnt]->getPhone();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                        </button>
                                    </form>
                                </td>

                                <!-- <td><?php //echo $customerDetails[$cnt]->getAction();?></td> -->

                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        $conn->close();
                        ?>
                    </tbody>

                </table>
            </div>
    </div>

    </form>

</div>

<style>
.second-level-li{
	color:#bf1b37;
	background-color:white;}
.second-level-li .hover1a{
	display:none;}
.second-level-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<!-- <script type="text/javascript">
$(document).ready(function()
{
    $("#divDataUpdated").load("adminDataUpdated2.php");
setInterval(function()
{
    $("#divDataUpdated").load("adminDataUpdated2.php");
}, 2000);
});
</script> -->

</body>
</html>
