<?php
//class User {
class SecondCustomerDetails {
    /* Member variables */
    // var $id, $name, $phone, $email, $teleName, $status, $remark, $lastUpdated ,$noOfCall, $updateStatus, $dateCreated, $dateUpdated;

    var $id,$name,$phone,$email,$teleName,$previousTele,$previousStatus,$status,$remark,$companyName,$previousType,$previousReason,$type,$reason,$recording,$remarkB,
            $occupation,$hobby,$location,$lastUpdated,$noOfCall,$updateStatus,$action,$filter,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTeleName()
    {
        return $this->teleName;
    }

    /**
     * @param mixed $teleName
     */
    public function setTeleName($teleName)
    {
        $this->teleName = $teleName;
    }

    /**
     * @return mixed
     */
    public function getPreviousTele()
    {
        return $this->previousTele;
    }

    /**
     * @param mixed $previousTele
     */
    public function setPreviousTele($previousTele)
    {
        $this->previousTele = $previousTele;
    }

    /**
     * @return mixed
     */
    public function getPreviousStatus()
    {
        return $this->previousStatus;
    }

    /**
     * @param mixed $previousStatus
     */
    public function setPreviousStatus($previousStatus)
    {
        $this->previousStatus = $previousStatus;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getPreviousType()
    {
        return $this->previousType;
    }

    /**
     * @param mixed $previousType
     */
    public function setPreviousType($previousType)
    {
        $this->previousType = $previousType;
    }

    /**
     * @return mixed
     */
    public function getPreviousReason()
    {
        return $this->previousReason;
    }

    /**
     * @param mixed $previousReason
     */
    public function setPreviousReason($previousReason)
    {
        $this->previousReason = $previousReason;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getRecording()
    {
        return $this->recording;
    }

    /**
     * @param mixed $recording
     */
    public function setRecording($recording)
    {
        $this->recording = $recording;
    }

    /**
     * @return mixed
     */
    public function getRemarkB()
    {
        return $this->remarkB;
    }

    /**
     * @param mixed $remarkB
     */
    public function setRemarkB($remarkB)
    {
        $this->remarkB = $remarkB;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param mixed $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return mixed
     */
    public function getHobby()
    {
        return $this->hobby;
    }

    /**
     * @param mixed $hobby
     */
    public function setHobby($hobby)
    {
        $this->hobby = $hobby;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return mixed
     */
    public function getNoOfCall()
    {
        return $this->noOfCall;
    }

    /**
     * @param mixed $noOfCall
     */
    public function setNoOfCall($noOfCall)
    {
        $this->noOfCall = $noOfCall;
    }

    /**
     * @return mixed
     */
    public function getUpdateStatus()
    {
        return $this->updateStatus;
    }

    /**
     * @param mixed $updateStatus
     */
    public function setUpdateStatus($updateStatus)
    {
        $this->updateStatus = $updateStatus;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param mixed $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getSecCustomerDetails($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","phone","email","tele_name","previous_tele","previous_status","status","remark","company_name","previous_type","previous_reason",
                                "type","reason","recording","remark_two","occupation","hobby","location","last_updated","no_of_call","update_status","action","filter",
                                    "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"second_customer_details");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$name,$phone,$email,$teleName,$previousTele,$previousStatus,$status,$remark,$companyName,$previousType,$previousReason,$type,$reason,
                                $recording,$remarkB,$occupation,$hobby,$location,$lastUpdated,$noOfCall,$updateStatus,$action,$filter,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new SecondCustomerDetails;
            $class->setId($id);
            $class->setName($name);
            $class->setPhone($phone);
            $class->setEmail($email);
            $class->setTeleName($teleName);
            $class->setPreviousTele($previousTele);
            $class->setPreviousStatus($previousStatus);
            $class->setStatus($status);
            $class->setRemark($remark);
            $class->setCompanyName($companyName);
            $class->setPreviousType($previousType);
            $class->setPreviousReason($previousReason);
            $class->setType($type);
            $class->setReason($reason);
            $class->setRecording($recording);
            $class->setRemarkB($remarkB);
            $class->setOccupation($occupation);
            $class->setHobby($hobby);
            $class->setLocation($location);
            $class->setLastUpdated($lastUpdated);
            $class->setNoOfCall($noOfCall);
            $class->setUpdateStatus($updateStatus);
            $class->setAction($action);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
