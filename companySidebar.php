<div class="side-bar red-bg">
	<div class="width100 text-center">
    	<img src="img/logo.png" class="logo-img" alt="logo" title="logo">
    </div>
	<ul class="sidebar-ul">
    	<a href="companySidebar.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>

        <!-- <a href="teleEditProfile.php"> -->
        <a href="companyEditProfile.php">
        	<li class="sidebar-li editpro-li hover1">
            	<img src="img/edit-profile2.png" class="hover1a" alt="Edit Profile" title="Edit Profile">
                <img src="img/edit-profile.png" class="hover1b" alt="Edit Profile" title="Edit Profile">
                 <p>Edit Profile</p>
            </li>
        </a>
        <!-- <a href="teleEditPassword.php"> -->
        <a href="companyEditPassword.php">
        	<li class="sidebar-li password-li hover1">
            	<img src="img/password2.png" class="hover1a" alt="Edit Password" title="Edit Password">
                <img src="img/password.png" class="hover1b" alt="Edit Password" title="Edit Password">
                 <p>Edit Password</p>
            </li>
        </a>

        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <img src="img/logo.png" class="mobile-logo" alt="Logo" title="Logo">
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <a href="teleDashboard.php">
                    <li class="sidebar-li dashboard-li hover1">
                        <img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Dashboard</p>
                    </li>
                </a>
                <a href="teleEditProfile.php">
                    <li class="sidebar-li editpro-li hover1">
                        <img src="img/edit-profile2.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/edit-profile.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Edit Profile</p>
                    </li>
                </a>
                 <a href="teleEditPassword.php">
                    <li class="sidebar-li password-li hover1">
                        <img src="img/password2.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/password.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Edit Password</p>
                    </li>
                </a>                
                <a href="logout.php">
                    <li class="sidebar-li hover1">
                        <img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                        <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
                         <p>Logout</p>
                    </li>
               </a>
            </ul>
    </div>
</header>