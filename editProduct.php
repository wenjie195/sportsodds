<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="adminTele | teleDashboard" />
    <title>adminTele | teleDashboard</title>
    <meta property="og:description" content="adminTele | teleDashboard" />
    <meta name="description" content="adminTele | teleDashboard" />
    <meta name="keywords" content="adminTele | teleDashboard">
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
  <form method="POST" action="utilities/editProductFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	Customer ID : <?php echo $_POST['customer_id']; ?>
        </a>
    </h1>

    <table class="edit-profile-table password-table">
        <?php
            if(isset($_POST['customer_id']))
            {
                $conn = connDB();
                $customerDetails = getCustomerDetails($conn,"WHERE id = ? ", array("id") ,array($_POST['customer_id']),"i");
            ?>

                <tr class="profile-tr">
                    <td class="">Name</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $customerDetails[0]->getName();?></td>
                </tr>

                <tr class="profile-tr">
                    <td>Phone</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $customerDetails[0]->getPhone();?></td>
                </tr>

                <tr class="profile-tr">
                    <td class="">Email</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $customerDetails[0]->getEmail();?></td>
                </tr>

                <tr class="profile-tr">
                    <td>Status</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $customerDetails[0]->getStatus();?></td>
                </tr>

                <tr class="profile-tr">
                    <td>Status</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                            <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Status" id="status" name="status">
                        </div>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td class="">Remark</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $customerDetails[0]->getRemark();?></td>
                </tr>

                <tr class="profile-tr">
                    <td>Remark</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                            <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="remark" id="remark" name="remark">
                        </div>
                    </td>
                </tr>

                <input type="hidden" placeholder="Product Id" id="id" name="id" value="<?php echo $productArray[0]->getId() ?>">
            
            <?php
            }
        ?>
    </table>

    <div class="clear"></div>

    <div class="three-btn-container">
        <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>
    </div>

</div>
</form>
</div>

</body>
</html>
