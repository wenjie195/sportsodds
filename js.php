<!-- <audio id="myAudio">
  <source src="css/noti2.ogg" type="audio/ogg">
  <source src="css/noti2.mp3" type="audio/mpeg">
 
</audio>
<div class="rrpowered_notification"></div> -->



<!-- Status Modal -->
<!-- <div id="status-modal" class="modal-css">
  <div class="modal-content-css status-content">
    <span class="close-css close-status">&times;</span>
    <h2 class="h2-title text-center">Add Status/Reason</h2>
    <form action="utilities/addStatusReasonFunction.php" method="POST">
    	<p class="input-p">Status</p>
        <select class="clean tele-input white-bg-input-ow" id="add_status" name="add_status" required>
        <option value="">Select a Status</option>
          <?php 
          //for ($cntPro=0; $cntPro <count($statusDetails) ; $cntPro++)
          {
          ?>
          <option value="<?php //echo $statusDetails[$cntPro]->getStatus();?>"> 
          <?php //echo $statusDetails[$cntPro]->getStatus(); ?>
          </option>
          <?php
          }
          ?>
        </select>

    	<p class="input-p">Reason</p>
        <input class=" tele-input white-bg-input-ow clean" type="text" placeholder="Reason" id="add_reason" name="add_reason" required>           
        
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean red-btn text-center modal-submit">Submit</button>
        </div>
    </form>
  </div>
</div> -->



<!-- Status Modal -->
<div id="company-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css status-content">
    <span class="close-css close-company">&times;</span>
    <h2 class="h2-title text-center">Add Company</h2>
    
    <!-- <form> -->
    <form action="utilities/addNewCompanyFunction.php" method="POST">
    	<p class="input-p">New Company Name</p>
        <input class="input-style clean" type="text" placeholder="Company Name" id="company_name" name="company_name" required>           
        
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean red-btn text-center modal-submit">Submit</button>
        </div>
    </form>
  </div>

</div>



<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="js/notification.js"></script>
<script src="js/notiflix-aio-1.9.1.js"></script>
<script src="js/notiflix-aio-1.9.1.min.js"></script>
<script src="js/rrpowered_notification_script.js"></script>
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>

<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
<!--- Modal Box --->
<script>
var statusmodal = document.getElementById("status-modal");

var companymodal = document.getElementById("company-modal");

var openstatus = document.getElementsByClassName("open-status")[0];

var closestatus = document.getElementsByClassName("close-status")[0];

var opencompany = document.getElementsByClassName("open-company")[0];

var closecompany = document.getElementsByClassName("close-company")[0];

if(openstatus){
openstatus.onclick = function() {
  statusmodal.style.display = "block";
}
}

if(closestatus){
closestatus.onclick = function() {
  statusmodal.style.display = "none";
}
}

if(opencompany){
  opencompany.onclick = function() {
  companymodal.style.display = "block";
}
}

if(closecompany){
  closecompany.onclick = function() {
  companymodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == statusmodal) {
    statusmodal.style.display = "none";
  }       
}

window.onclick = function(event) {
  if (event.target == companymodal) {
    companymodal.style.display = "none";
  }       
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>