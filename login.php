<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="adminTele" />
    <title>adminTele</title>
    <meta property="og:description" content="adminTele" />
    <meta name="description" content="adminTele" />
    <meta name="keywords" content="adminTele">
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->

</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">Login</h1>

    <div class="clear"></div>
    
        <form  class="edit-profile-div2" action="utilities/loginFunction.php" method="POST">
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <td class="">Username</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Username" id="username" name="username" required>
                    </td>
                </tr>
                <tr class="profile-tr">
                    <td>Password</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Password" id="password" name="password" required>
                    </td>
                </tr>
            </table>

            <div class="clear"></div>

            <button class="confirm-btn text-center white-text clean gold-button"name="loginButton">Login</button>
        </form>
        
</div>

</body>
</html>