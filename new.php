<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$page = $_SERVER['PHP_SELF'];
$sec = "10";

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');

//additional hrs or mins
//add 1 hour to time
$cenvertedTime = date('Y-m-d H:i:s',strtotime('+1 hour',strtotime($time)));
// echo 'Converted Time (added 1 hour): '.$cenvertedTime;
//30 minutes to time (show previous 30mins got how many customer update as Good)
$cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-1 minutes',strtotime($time)));

// $customerDetails = getCustomerDetails($conn," WHERE no_of_call <=2 AND status != 'Good' LIMIT 500");
// $customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND status != 'Good' LIMIT 500");

$qwerty = '2020-02-05 17:43:45';
// $asd = getCustomerDetails($conn," WHERE date_created ='2020-02-05 12:26:33' ");
$asd = getCustomerDetails($conn," WHERE status = 'Good' AND date_updated = '".$qwerty."' ");
$aassdd = getTimeTeleUpdate($conn," WHERE update_status = 'Good' AND date_created >= '".$cenvertedTimeMin."' ");


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Customer Details | adminTele" />
    <!-- <meta http-equiv="refresh" content="<?php //echo $sec?>;URL='<?php //echo $page?>'"> -->
    <title>Customer Details | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'js.php'; ?>
    <?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
        $(function(){
        $("#fromDate").datepicker(
            {
                dateFormat:'yy-mm-dd',
                changeMonth: true,
                changeYear:true,
            }

        );
        });
    </script>
    <script>
        $(function(){
        $("#toDate").datepicker(
            {
                dateFormat:'yy-mm-dd',
                changeMonth: true,
                changeYear:true,
            }

        );
        });
    </script>
	  <?php include 'css.php'; ?>

    <!-- Script -->
    <script type='text/javascript'>
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
</head>

<body class="body">

<!-- <//?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?> -->
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">Customer</h1>

    <div class="clear"></div>

    <?php
    if($asd)
    {
        $totalMember = count($asd);
    }
    else
    {   $totalMember = 0;   }
    ?>

    <div class="clear"></div>

    <?php
    if($aassdd)
    {
        $updates = count($aassdd);
    }
    else
    {   $updates = 0;   }
    ?>

    <!-- <div class="clear"></div>

    <?php //echo $time;?>

    <div class="clear"></div>

    <?php //echo $qwerty;?>

    <div class="clear"></div>

    <?php //echo $cenvertedTime;?>

    <div class="clear"></div>

    <?php //echo $cenvertedTimeMin;?>

    <div class="clear"></div> -->

    <h4 class="tab-h2">Customer Details | <a href="checkLogUpdated.php" class="red-link">Customer Details (Updated)</a> | <a href="checkLogGood.php" class="red-link">Customer Details (Good)</a> | <a href="checkLogBlack.php" class="red-link">Customer Details (Black List)</a> </h4>

    <div class="clear"></div>

    <!-- Search filter -->
    <form method='post' action=''> 
    <?php
    if(isset($_POST['reset'])){
      ?>
      From Date <input type='text' class='dateFilter' name='fromDate'>
      To Date <input type='text' class='dateFilter' name='endDate' >
    <?php
    }else{
      ?>
      From Date <input type='text' class='dateFilter' name='fromDate' value='<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>'>
      To Date <input type='text' class='dateFilter' name='endDate' value='<?php if(isset($_POST['endDate'])) echo $_POST['endDate']; ?>'>
    <?php
    }
    ?>
      <input type='submit' name='but_search' value='Search'>
      <input type='submit' name='reset' value='reset'>
    </form>
    <div style="clear:both"></div>
    </br>

    <div class="col-md-3">
        <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" placeholder="Search">
    </div>
    <div class="col-md-3">
        <input type="text" id="myInputA" onkeyup="myFunctionA()" class="form-control" placeholder="Search">
    </div>
    <div style="clear:both"></div>
    </br>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div" id="userDetail">
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>STATUS</th>
                            <th>REMARK</th>
                            <th>LAST UPDATED</th>
                            <th>REVIEW</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        // Date filter
                        if(isset($_POST['but_search'])){
                            $fromDate = $_POST['fromDate'];
                            $endDate = $_POST['endDate'];
                            
                            if(!empty($fromDate) && !empty($endDate)){
                              $customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND status != 'Good' and last_updated between '".$fromDate."' and '".$endDate."'");
                            }
                        }elseif (isset($_POST['reset'])) {
                          $customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND status != 'Good' LIMIT 500");
                        }
                        
                        else{
                          $customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND status != 'Good' LIMIT 500");
                        }

                        if($customerDetails)
                        {
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td><?php //echo $customerDetails[$cnt]->getId();?></td> -->
                                <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                                <td><?php echo $customerDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $customerDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $customerDetails[$cnt]->getRemark();?></td>
                                <td><?php echo date("Y-m-d",strtotime($customerDetails[$cnt]->getLastUpdated()));?></td>

                                <td>
                                    <form action="reviewCustomerDetails.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="customer_name" value="<?php echo $customerDetails[$cnt]->getPhone();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>


</div>

<style>
.customer-li{
	color:#bf1b37;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionA() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputA");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[6];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<!-- <//?php include 'js.php'; ?> -->
</body>
</html>