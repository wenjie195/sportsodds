-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2020 at 09:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_admintele`
--

-- --------------------------------------------------------

--
-- Table structure for table `companyselection`
--

CREATE TABLE `companyselection` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companyselection`
--

INSERT INTO `companyselection` (`id`, `company_name`, `date_created`, `date_updated`) VALUES
(1, 'KC', '2020-02-23 19:01:06', '2020-04-23 08:01:15'),
(2, 'SW', '2020-02-23 19:01:06', '2020-04-23 08:01:19'),
(3, '-', '2020-02-23 19:01:28', '2020-06-02 06:57:18'),
(4, 'Company D', '2020-02-23 19:01:28', '2020-02-23 19:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tele_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `recording` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `last_updated` varchar(255) DEFAULT NULL,
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `filter` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `emgc_sos`
--

CREATE TABLE `emgc_sos` (
  `id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` int(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emgc_sos`
--

INSERT INTO `emgc_sos` (`id`, `link`, `type`, `date_created`, `date_updated`) VALUES
(1, '<script>window.location=\'../teleDashboard.php\'</script>', 1, '2020-01-17 15:35:22', '2020-03-09 01:04:56'),
(2, 'header(\'Location: https://bigdomain.my\');', 2, '2020-01-17 15:44:58', '2020-01-17 15:44:58'),
(3, '  ?><?php echo $userDetails->getLink();?><?php', 3, '2020-01-17 15:51:10', '2020-01-17 15:51:26'),
(4, 'header(\'Location: https://www.facebook.com\');', 4, '2020-01-17 15:51:10', '2020-01-17 15:51:28'),
(5, 'echo \"<meta http-equiv=Refresh content=1;url=https://bigdomain.my/>\";', 5, '2020-01-17 15:53:20', '2020-01-17 15:53:20'),
(6, '<meta http-equiv=Refresh content=0;url=https://amway.my/>;', 6, '2020-01-17 15:56:40', '2020-01-17 16:13:23'),
(7, 'echo \"<script>window.location=\'../teleDashboard.php\'</script>\";', 7, '2020-01-17 16:11:04', '2020-01-17 16:11:04'),
(8, '<script>window.location=\'../teleDashboard.php\'</script>', 8, '2020-03-06 02:50:41', '2020-03-06 02:50:47'),
(9, '<script>window.location=\'../companySelection.php\'</script>', 9, '2020-03-09 01:04:47', '2020-03-09 01:04:47');

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notepad`
--

CREATE TABLE `notepad` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `type` int(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notepad`
--

INSERT INTO `notepad` (`id`, `uid`, `title`, `content`, `author_uid`, `author_name`, `type`, `date_created`, `date_updated`) VALUES
(1, '8680bde6f0c602f51e006a83d355b0ac', '1234567', 'qwerty\r\nasd\r\nzxc', 'db2d9264afd412c9a2fa47af0579a4f0', 'kent', 1, '2020-08-12 02:12:44', '2020-08-12 02:12:44'),
(2, '8bd78129825228c5c974de2c98403ba5', '112233', 'aaa\r\nbbb\r\nccc\r\nddd\r\neee\r\nfff\r\nggg\r\nhhh\r\niii\r\njjj\r\nkkk\r\nlll\r\nmmm\r\nnnn\r\nooo\r\nppp\r\nqqq\r\nrrr\r\nsss\r\nttt\r\nuuu\r\nvvv\r\nwww', 'db2d9264afd412c9a2fa47af0579a4f0', 'kent', 1, '2020-08-12 02:13:09', '2020-08-12 02:15:13'),
(3, '6fb8e9125ff1d5503bdfbd6603d73094', '2324', 'qqwweerrttyy\r\nqwerty\r\naassdd\r\nasd\r\nzxc', 'db2d9264afd412c9a2fa47af0579a4f0', 'kent', 4, '2020-08-12 02:13:38', '2020-08-12 02:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `reason`
--

CREATE TABLE `reason` (
  `id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `type` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reason`
--

INSERT INTO `reason` (`id`, `status`, `reason`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Good', 'Very interesting', 2, '2020-02-19 20:00:28', '2020-06-16 06:45:16'),
(2, 'Good', 'Interested', 1, '2020-02-19 20:00:28', '2020-04-23 07:52:01'),
(3, 'Good', 'Normal ', 1, '2020-02-19 20:01:10', '2020-04-23 07:52:08'),
(4, 'Bad', 'Lose Many Ady', 1, '2020-02-20 17:26:18', '2020-04-23 07:52:44'),
(5, 'Bad', 'Dun Know Game', 1, '2020-02-19 20:01:39', '2020-06-16 06:46:23'),
(6, 'Bad', 'Direct close', 1, '2020-02-19 20:01:39', '2020-04-23 07:53:39'),
(7, 'Other', 'Currently Not Interesting', 1, '2020-02-19 20:02:16', '2020-04-23 07:54:58'),
(8, 'Other', 'Outstation', 1, '2020-02-19 20:02:16', '2020-04-23 07:55:36'),
(9, 'Other', 'Other 3', 2, '2020-02-19 20:03:00', '2020-04-23 07:57:39'),
(10, 'No Take Call', 'No Take Call', 1, '2020-02-19 20:03:00', '2020-04-23 07:57:52'),
(11, 'No Take Call', 'No Take Call 2', 2, '2020-02-19 20:03:20', '2020-04-23 07:53:51'),
(12, 'No Take Call', 'No Take Call 3', 2, '2020-02-19 20:03:20', '2020-04-23 07:53:54'),
(13, 'Bad', 'No Service', 1, '2020-04-23 07:57:23', '2020-06-16 07:05:21'),
(14, 'Good', 'sw', 2, '2020-06-01 13:24:15', '2020-06-16 06:44:57'),
(15, 'Bad', 'sw', 2, '2020-06-01 13:24:22', '2020-06-16 06:45:00'),
(16, 'Good', 'kc', 2, '2020-06-10 10:26:39', '2020-06-16 06:45:07'),
(17, 'Good', 'Ask Whatsapp only', 1, '2020-06-16 06:46:02', '2020-06-16 06:46:02'),
(18, 'Bad', 'same occupation', 1, '2020-06-16 07:29:38', '2020-06-16 07:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `second_customer_details`
--

CREATE TABLE `second_customer_details` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tele_name` varchar(255) DEFAULT NULL,
  `previous_tele` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `recording` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `last_updated` varchar(255) DEFAULT NULL,
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `second_customer_details`
--

INSERT INTO `second_customer_details` (`id`, `name`, `phone`, `email`, `tele_name`, `previous_tele`, `status`, `remark`, `company_name`, `type`, `reason`, `recording`, `remark_two`, `occupation`, `hobby`, `location`, `last_updated`, `no_of_call`, `update_status`, `action`, `date_created`, `date_updated`) VALUES
(1, '', '0129128934', NULL, 'sky', NULL, NULL, NULL, 'SW', 'Good', 'Interested', NULL, NULL, NULL, NULL, ' - ', '2020-06-09 18:22:29', '0', NULL, NULL, '2020-06-09 10:22:29', '2020-06-09 10:22:29'),
(2, '', '0129129421', NULL, 'sky', NULL, NULL, NULL, 'KC', 'Good', 'Normal ', NULL, NULL, NULL, NULL, ' - ', '2020-06-09 18:54:32', '0', NULL, NULL, '2020-06-09 10:54:32', '2020-06-09 10:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` int(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`id`, `link`, `type`, `date_created`, `date_updated`) VALUES
(1, '<meta http-equiv=Refresh content=0;url=https://amway.my/>;', 0, '2020-01-17 07:35:22', '2020-02-04 08:24:55'),
(2, '<url=https://bigdomain.my/>', 8, '2020-01-17 07:44:58', '2020-03-06 02:47:58'),
(8, '\"$_SERVER[\'PHP_SELF\']\"', 3, '2020-02-04 08:30:29', '2020-02-04 08:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Good', 1, '2020-02-19 20:00:28', '2020-02-19 20:00:28'),
(2, 'Bad', 1, '2020-02-19 20:01:10', '2020-02-19 20:36:28'),
(3, 'Other', 1, '2020-02-19 20:02:16', '2020-02-19 20:36:47'),
(4, 'No Take Call', 1, '2020-02-19 20:03:00', '2020-02-19 20:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `time_teleupdate`
--

CREATE TABLE `time_teleupdate` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` text DEFAULT NULL,
  `update_status` varchar(255) DEFAULT NULL,
  `update_remark` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `recording` varchar(255) DEFAULT NULL,
  `remark_two` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `no_of_update` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 'admin', NULL, NULL, 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41'),
(2, 'db2d9264afd412c9a2fa47af0579a4f0', 'kent', 'kc66@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '01127502809', 'kent', 'Suntech', NULL, 1, 1, '2020-03-28 12:05:58', '2020-08-11 05:57:41'),
(3, '348fbe29a005bef943d11c66a0955be8', 'weisheng', 'kuatcuci66@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '0147458524', 'weisheng', 'suntech', NULL, 1, 1, '2020-05-21 09:28:43', '2020-08-11 05:57:43'),
(4, '06fe427c8bf815154f14ec870a7fe555', 'weixin', 'kuatcuci@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '0174208129', 'weixin', 'suntech', NULL, 1, 1, '2020-05-22 08:10:17', '2020-08-11 05:57:45'),
(5, '5c56e2de12a6221bc4a8ddbab87f2176', 'amy', 'skywin66@gmail.com', '524af6027194c989a3199620dd468b053f251da88ade2d8c9f8a0001862885bb', '414d63457f6f0589850314f3cfb384cde622b44d', '0173832548', 'amy', 'Suntech', NULL, 1, 1, '2020-06-04 03:12:16', '2020-06-16 07:03:56'),
(6, '42173efad0c8e98d00b5fa77300b40b9', 'kenee', 'conyang@gmail.com', '6611a82a3ae564b90095c053fa412672948794a7a0bfcf7fb9d458c377299e11', 'f9757e146cd05a2650692a8097b4df97339a4b35', '0184052272', 'kenee', 'Suntech', NULL, 1, 0, '2020-06-16 06:55:01', '2020-06-16 06:55:01'),
(7, 'eee9b6a2053f893af85a5db43b1552e8', 'adeline', 'adeline@gmail.com', 'ae3132beb827e14896210c14be0d61e829b620f26b4459059c4b35fc01400325', '52123696747dcb4ee83eee6e5daa4c2f908b57fc', '0178715560', 'adeline', 'Suntech', NULL, 1, 1, '2020-06-22 02:34:11', '2020-06-22 03:06:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companyselection`
--
ALTER TABLE `companyselection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emgc_sos`
--
ALTER TABLE `emgc_sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notepad`
--
ALTER TABLE `notepad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reason`
--
ALTER TABLE `reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `second_customer_details`
--
ALTER TABLE `second_customer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companyselection`
--
ALTER TABLE `companyselection`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customerdetails`
--
ALTER TABLE `customerdetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emgc_sos`
--
ALTER TABLE `emgc_sos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notepad`
--
ALTER TABLE `notepad`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reason`
--
ALTER TABLE `reason`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `second_customer_details`
--
ALTER TABLE `second_customer_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
