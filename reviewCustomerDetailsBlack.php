<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Review Customer Details | adminTele" />
    <title>Review Customer Details | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">
  <!-- <form method="POST" action="utilities/editProductFunction.php" enctype="multipart/form-data"> -->
  <form method="POST" action="utilities/updateCustomerDetailsFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	Customer Phone No. : <?php echo $_POST['customer_name']; ?>
        </a>
    </h1>

    <!-- <h4><?php //echo $userDetails->getUsername();?></h4>
    <h4><?php //echo $userDetails->getUid();?></h4> -->

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">

                <?php
                    if(isset($_POST['customer_name']))
                    {
                        $conn = connDB();
                        $customerDetails = getTimeTeleUpdate($conn,"WHERE customer_phone = ? ", array("customer_phone") ,array($_POST['customer_name']),"s");
                    ?>

                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>TELE NAME</th>
                            <th>UPDATE STATUS</th>
                            <th>UPDATE REMARK</th>

                            <th>COMPANY TYPE</th>
                            <th>STATUS</th>
                            <th>REASON</th>

                            <th>REMARK 2</th>
                            <th>OCCUPATION</th>

                            <th>RECORDING</th>

                            <th>DATE UPDATE</th>
                        </tr>
                    </thead>

                    <!-- <tbody>
                    </tbody> -->

                    <tbody>
                        <?php

                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $customerDetails[$cnt]->getTeleName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getUpdateStatus();?></td>
                                <td><?php echo $customerDetails[$cnt]->getUpdateRemark();?></td>

                                <td><?php echo $customerDetails[$cnt]->getCompanyName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getType();?></td>
                                <td><?php echo $customerDetails[$cnt]->getReason();?></td>

                                <td><?php echo $customerDetails[$cnt]->getRemarkB();?></td>
                                <td><?php echo $customerDetails[$cnt]->getOccupation();?></td>

                                <td>

                                    <audio controls>
                                        <source src="upload_recording/<?php echo $customerDetails[$cnt]->getRecording();?>" type="audio/mp3">
                                    </audio>

                                </td>

                                <td><?php echo $customerDetails[$cnt]->getDateCreated();?></td>
                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                    <?php
                    }
                ?>

                </table>
            </div>
    </div>

    <div class="clear"></div>

</div>
</form>
</div>
<style>
.customer-li{
	color:#bf1b37;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
