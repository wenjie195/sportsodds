<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/TimeTeleUpdate.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// $comp = $_SESSION['company'];

$conn = connDB();

// $page = $_SERVER['PHP_SELF'];
// $sec = "12";
//testing updates
// $sec = "4";

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');
//additional hrs or mins
//add 1 hour to time / 30mins
$cenvertedTime = date('Y-m-d H:i:s',strtotime('+1 hour',strtotime($time)));
$cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-4 seconds',strtotime($time)));
$aassdd = getTimeTeleUpdate($conn," WHERE update_status = 'Good' AND date_created >= '".$cenvertedTimeMin."' ");

$qwerty = getTimeTeleUpdate($conn," WHERE uid = ? AND update_status = 'Good' AND date_created >= '".$cenvertedTimeMin."' ",array("uid"),array($uid),"s");

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$teleName = $userDetails -> getUsername();

// $customerDetails = getCustomerDetails($conn," WHERE tele_name = ? AND status != 'Good' ",array("tele_name"),array($teleName),"s");
// $customerDetails = getCustomerDetails($conn," WHERE tele_name = ? AND no_of_call <=2 AND status != 'Good' LIMIT 500 ",array("tele_name"),array($teleName),"s");
$customerDetails = getCustomerDetails($conn," WHERE tele_name = ? AND no_of_call > 0 AND no_of_call <= 2 AND status != 'Good' LIMIT 4000 ",array("tele_name"),array($teleName),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Telemarketer Dashboard | adminTele" />
    <!-- <meta http-equiv="refresh" content="<?php //echo $sec?>;URL='<?php //echo $page?>'"> -->
    <title>Telemarketer Dashboard | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">Telemarketer Dashboard</h1>
    
    <div class="clear"></div>

    <?php
    if($aassdd)
    {   
        $updates = count($aassdd);
    }
    else
    {   $updates = 0;   }
    ?>

    <div class="clear"></div>

    <?php
    if($qwerty)
    {   
        $updates2 = count($qwerty);
    }
    else
    {   $updates2 = 0;   }
    ?>

    <div class="clear"></div>

    <!-- <h2 class="tab-h2">Customer Details | <a href="teleDashboardBlack.php" class="red-link">Customer Details (Black List)</a> </h2> -->
    <h4 class="tab-h2"><a href="teleDashboard.php" class="red-link">Customer Details</a> | Customer Details (Updated) | <a href="teleDashboardGood.php" class="red-link">Customer Details (Good)</a> | <a href="teleDashboardBlack.php" class="red-link">Customer Details (Black List)</a> </h4>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>STATUS</th>
                            <th>REMARK</th>
                            <th>LAST UPDATED</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td><?php //echo $customerDetails[$cnt]->getId();?></td> -->
                                <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                                <td><?php echo $customerDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $customerDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $customerDetails[$cnt]->getRemark();?></td>

                                <!-- <td><?php //echo $customerDetails[$cnt]->getLastUpdated();?></td> -->
                                <td><?php echo date("d-m-Y",strtotime($customerDetails[$cnt]->getLastUpdated()));?></td>

                                <!-- <td>
                                    <form action="editProduct.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="customer_id" value="<?php //echo $customerDetails[$cnt]->getId();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit2.png" class="edit-announcement-img hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td> -->

                                <td>
                                    <form action="updateCustomerDetails.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="customer_id" value="<?php echo $customerDetails[$cnt]->getId();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Update">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Update">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>

        
</div>
<style>
.dashboard-li{
	color:#bf1b37;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>