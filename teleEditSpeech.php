<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Notepad.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $teleName = $userDetails -> getUsername();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Speech Draft | adminTele" />
    <title>Edit Speech Draft | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Edit Speech Draft</h1> 

    <?php
    if(isset($_POST['notepad_uid']))
    {
    $conn = connDB();
    $notepadDetails = getNotepad($conn,"WHERE uid = ? ", array("uid") ,array($_POST['notepad_uid']),"s");
    ?>

        <form action="utilities/editNotesFunction.php" method="POST">

            <div class="width100">
                <p class="input-title-p">Title</p>
                <input class="clean tele-input" type="text" placeholder="Title" value="<?php echo $notepadDetails[0]->getTitle();?>" name="update_title" id="update_title">        
            </div> 
            <div class="clear"></div>


            <div class="width100">
                <p class="input-title-p">Content</p>
                <textarea class="clean tele-input speech-textarea" placeholder="Speech Content" name="update_content" id="update_content"><?php echo $notepadDetails[0]->getContent();?></textarea>        
            </div>       

            <input class="clean tele-input" type="hidden" value="<?php echo $notepadDetails[0]->getUid();?>" name="notepad_uid" id="notepad_uid">  

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

        </form>

    <?php
    }
    ?>


        <!-- <form action="utilities/addNewRefereeFunction.php" method="POST"> -->
        <!-- <form action="utilities/editProfileFunction.php" method="POST">

            <div class="width100">
                <p class="input-title-p">Title</p>
                <input class="clean tele-input" type="text" placeholder="Title">        
            </div> 
            <div class="clear"></div>


            <div class="width100">
                <p class="input-title-p">Content</p>
                <textarea class="clean tele-input speech-textarea" placeholder="Speech Content"></textarea>        
            </div>       

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

        </form> -->

       
</div>
<style>
.speech-li{
	color:#bf1b37;
	background-color:white;}
.speech-li .hover1a{
	display:none;}
.speech-li .hover1b{
	display:block;}
</style>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<?php include 'js.php'; ?>
</body>
</html>