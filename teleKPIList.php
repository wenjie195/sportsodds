<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Telemarketer KPI | adminTele" />
    <title>Telemarketer KPI | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	KPI
        </a>
    </h1>
    
    <div class="clear"></div>

    <?php
    $conn = connDB();
    if(isset($_POST['tele_username']))
    {
    $conn = connDB();
    $customerDetails = getCustomerDetails($conn," WHERE tele_name = ? AND type != '' ",array("tele_name"),array($_POST['tele_username']),"s");

    $totalCall = getCustomerDetails($conn," WHERE tele_name = ? AND type != '' ",array("tele_name"),array($_POST['tele_username']),"s");
    $totalGoodCall = getCustomerDetails($conn," WHERE tele_name = ? AND type = 'Good' ",array("tele_name"),array($_POST['tele_username']),"s");
    $totalBadCall = getCustomerDetails($conn," WHERE tele_name = ? AND type = 'Bad' ",array("tele_name"),array($_POST['tele_username']),"s");
    $totalNTCCall = getCustomerDetails($conn," WHERE tele_name = ? AND type = 'No Take Call' ",array("tele_name"),array($_POST['tele_username']),"s");
    $totalOtherCall = getCustomerDetails($conn," WHERE tele_name = ? AND type = 'Other' ",array("tele_name"),array($_POST['tele_username']),"s");

    ?>

        <div class="big-four-input-container ow-margin-bottom-0">
            <form method='post' action='teleKPIListSearch.php'> 
                <div class="three-input-div">
                    <p class="input-top-p">From Date</p>
                    <input type='text' class='dateFilter tele-four-input tele-input clean' name='fromDate' id="fromDate">
                </div>
                <div class="three-input-div left-three-input">
                    <p class="input-top-p">To Date</p>
                    <input type='text' class='dateFilter tele-four-input tele-input clean' name='endDate' id="endDate">
                </div>    

                <input type='hidden' class='dateFilter tele-four-input tele-input clean' value="<?php echo $_POST['tele_username'];?>" name='teleName' id="teleName" readonly>

                <div class="three-input-div sub-div">
                    <input type='submit' name='but_search' value='Search' class="submit-btn clean">
                </div>
            </form>
        </div>

            <?php
            if($totalCall)
            {   
                $totalCallMake = count($totalCall);
            }
            else
            {   
                $totalCallMake = 0;   
            }
            ?>

            <?php
            if($totalGoodCall)
            {   
                $totalGoodCallMake = count($totalGoodCall);
            }
            else
            {   
                $totalGoodCallMake = 0;   
            }
            ?>

            <?php
            if($totalBadCall)
            {   
                $totalBadCallMake = count($totalBadCall);
            }
            else
            {   
                $totalBadCallMake = 0;   
            }
            ?>

            <?php
            if($totalNTCCall)
            {   
                $totalNTCCallMake = count($totalNTCCall);
            }
            else
            {   
                $totalNTCCallMake = 0;   
            }
            ?>

            <?php
            if($totalOtherCall)
            {   
                $totalOtherCallMake = count($totalOtherCall);
            }
            else
            {   
                $totalOtherCallMake = 0;   
            }
            ?>
            
        <table class="call-table">
        	<tbody>
            	
            	<tr><td><?php echo $totalCallMake; ?></td><td><img src="img/phone2.png" class="call-icon" alt="Total Call" title="Total Call"><b>Total Call</b></td></tr>
				<tr><td><?php echo $totalGoodCallMake; ?></td><td><img src="img/tick.png" class="call-icon" alt="Good" title="Good"><b class="green-text">Total Call (Good)</b><td></tr>
                <tr><td><?php echo $totalBadCallMake; ?></td><td><img src="img/close.png" class="call-icon" alt="Bad" title="Bad"><b class="pink-text">Total Call (Bad)</b></td></tr>
                <tr><td><?php echo $totalNTCCallMake; ?></td><td><img src="img/not-picked.png" class="call-icon" alt="No Take Call" title="No Take Call"><b class="orange-text">Total Call (No Take Call)</b></td></tr>
                <tr><td><?php echo $totalOtherCallMake; ?></td><td><img src="img/others.png" class="call-icon" alt="Total Call" title="Total Call"><b class="yellow-text">Total Call (Others)</b></td></tr>
                 
            </tbody>
       </table>

        <div class="clear"></div>

        <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>STATUS</th>
                            <th>REASON</th>
                            <th>LAST UPDATED</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                    <td><?php echo $customerDetails[$cnt]->getPhone();?></td>

                                    <td><?php echo $customerDetails[$cnt]->getType();?></td>
                                    <td><?php echo $customerDetails[$cnt]->getReason();?></td>

                                    <td>
                                        <?php echo $customerDetails[$cnt]->getLastUpdated();?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php
    $conn->close();
    }
    ?>
</div>

<style>
.telemarketer-li{
	color:#bf1b37;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#toDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

<script type='text/javascript'>
  $(document).ready(function()
  {
    $('.dateFilter').datepicker(
    {
      dateFormat: "yy-mm-dd"
    });
  });
</script>

</body>
</html>