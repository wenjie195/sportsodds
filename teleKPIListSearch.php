<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $fromDate = rewrite($_POST["fromDate"]);
    $endDate = rewrite($_POST["endDate"]);
    $teleName = rewrite($_POST["teleName"]);

    // echo $fromDate = rewrite($_POST["fromDate"]);
    // echo $endDate = rewrite($_POST["endDate"]);
    // echo $teleName = rewrite($_POST["teleName"]);

    $customerDetails = getCustomerDetails($conn, "WHERE tele_name = '$teleName' AND type != '' AND last_updated >= '$fromDate' AND last_updated <='$endDate'  ");

    $totalGoodCall = getCustomerDetails($conn," WHERE tele_name = '$teleName' AND type = 'Good' AND last_updated >= '$fromDate' AND last_updated <='$endDate' ");
    $totalBadCall = getCustomerDetails($conn," WHERE tele_name = '$teleName' AND type = 'Bad' AND last_updated >= '$fromDate' AND last_updated <='$endDate' ");
    $totalNTCCall = getCustomerDetails($conn," WHERE tele_name = '$teleName' AND type = 'No Take Call' AND last_updated >= '$fromDate' AND last_updated <='$endDate' ");
    $totalOtherCall = getCustomerDetails($conn," WHERE tele_name = '$teleName' AND type = 'Other' AND last_updated >= '$fromDate' AND last_updated <='$endDate' ");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Telemarketer KPI | adminTele" />
    <title>Telemarketer KPI | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title">KPI</h1>
    
    <div class="white-dropshadow-box four-div-box">
        <p class="four-div-p">Total Calls: 

        <?php
        if($customerDetails)
        {   
            $totalCustomerCall = count($customerDetails);
        }
        else
        {   $totalCustomerCall = 0;   }
        ?>
        <b class="total-call-b ow-black-text"><?php echo $totalCustomerCall;?></b> <img src="img/phone.png" class="call" alt="Call" title="Call">
        
        </p>
    </div>

    <?php
    if($totalGoodCall)
    {   
        $totalGoodCallMake = count($totalGoodCall);
    }
    else
    {   
        $totalGoodCallMake = 0;   
    }
    ?>

    <?php
    if($totalBadCall)
    {   
        $totalBadCallMake = count($totalBadCall);
    }
    else
    {   
        $totalBadCallMake = 0;   
    }
    ?>

    <?php
    if($totalNTCCall)
    {   
        $totalNTCCallMake = count($totalNTCCall);
    }
    else
    {   
        $totalNTCCallMake = 0;   
    }
    ?>

    <?php
    if($totalOtherCall)
    {   
        $totalOtherCallMake = count($totalOtherCall);
    }
    else
    {   
        $totalOtherCallMake = 0;   
    }
    ?>

    <table class="call-table">
        <tbody>
            <tr><td><?php echo $totalGoodCallMake; ?></td><td><img src="img/tick.png" class="call-icon" alt="Good" title="Good"><b class="green-text">Total Call (Good)</b><td></tr>
            <tr><td><?php echo $totalBadCallMake; ?></td><td><img src="img/close.png" class="call-icon" alt="Bad" title="Bad"><b class="pink-text">Total Call (Bad)</b></td></tr>
            <tr><td><?php echo $totalNTCCallMake; ?></td><td><img src="img/not-picked.png" class="call-icon" alt="No Take Call" title="No Take Call"><b class="orange-text">Total Call (No Take Call)</b></td></tr>
            <tr><td><?php echo $totalOtherCallMake; ?></td><td><img src="img/others.png" class="call-icon" alt="Total Call" title="Total Call"><b class="yellow-text">Total Call (Others)</b></td></tr>
        </tbody>
    </table>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>

                            <th>STATUS</th>
                            <th>REASON</th>

                            <th>LAST UPDATED</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                    <td><?php echo $customerDetails[$cnt]->getPhone();?></td>

                                    <td><?php echo $customerDetails[$cnt]->getType();?></td>
                                    <td><?php echo $customerDetails[$cnt]->getReason();?></td>

                                    <td>
                                        <?php echo $customerDetails[$cnt]->getLastUpdated();?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        <?php
                        }
                        $conn->close();
                        ?>
                    </tbody>
                </table>
            </div>
    </div>

</div>

<style>
.telemarketer-li{
	color:#bf1b37;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#toDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

<script type='text/javascript'>
  $(document).ready(function()
  {
    $('.dateFilter').datepicker(
    {
      dateFormat: "yy-mm-dd"
    });
  });
</script>

</body>
</html>