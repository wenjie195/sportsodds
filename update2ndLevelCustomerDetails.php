<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/SecondCustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Reason.php';
require_once dirname(__FILE__) . '/classes/Status.php';
require_once dirname(__FILE__) . '/classes/CompanySelection.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $comp = $_SESSION['company'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$statusDetails = getStatus($conn);
// $reasonDetails = getReason($conn);
$reasonDetails = getReason($conn," WHERE type = 1 ");
$companyDetails = getCompanySelection($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Update Customer Details | adminTele" />
    <title>Update Customer Details | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
    <?php include 'autolog.php' ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>

<div class="next-to-sidebar">
    
    <form method="POST" action="utilities/update2ndLevelCustomerDetailsFunction.php" enctype="multipart/form-data">
    <!-- <form method="POST" action="utilities/updateCustomerDetailsFunction.php" enctype="multipart/form-data"> -->
    <!-- <form  action="utilities/updateVoiceFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form"> -->

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Customer ID : <?php echo $_POST['customer_id']; ?>
        </a>
    </h1>

    <?php
    if(isset($_POST['customer_id']))
    {
        $conn = connDB();
        $customerDetails = getSecCustomerDetails($conn,"WHERE id = ? ", array("id") ,array($_POST['customer_id']),"i");
        ?>

        <div class="input50-div">
            <p class="input-title-p">Name</p>
            <input class="clean tele-input" id="update_name" value="<?php echo $customerDetails[0]->getName();?>" name="update_name">    
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Phone</p>
            <p class="clean tele-input clean-bg no-input-style"><?php echo $customerDetails[0]->getPhone();?></p>       
        </div> 

        <div class="clear"></div>

        <!-- <div class="input50-div">
            <p class="input-title-p">Email</p>
            <p class="clean tele-input clean-bg"><?php //echo $customerDetails[0]->getEmail();?></p>       
        </div>  -->

        <div class="input50-div">
            <p class="input-title-p">Company</p>
            <!-- <input class="clean tele-input" type="text" placeholder="Company" id="update_company" name="update_company"> -->
            <select class="clean tele-input" id="update_company" value="<?php echo $companyDetails[0]->getCompanyName();?>" name="update_company">
                <option value="">Please Select a Company</option>
                <?php
                for ($cntPro=0; $cntPro <count($companyDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $companyDetails[$cntPro]->getCompanyName(); ?>"> 
                        <?php echo $companyDetails[$cntPro]->getCompanyName(); ?>
                        <!--take in display the options-->
                    </option>
                <?php
                }
                ?>
            </select>
        </div>

        <div class="input50-div second-input50">
            <p class="input-title-p">Status</p>
            <select class="clean tele-input" id="update_status" value="<?php echo $customerDetails[0]->getStatus();?>" name="update_status">
            <?php
            if($customerDetails[0]->getStatus() == '')
            {
            ?>
                <option value="Bad"  name='Bad'>Bad</option>
                <option value="Good"  name='Good'>Good</option>
                <option selected value=""  name=''></option>
            <?php
            }
            else if($customerDetails[0]->getStatus() == 'Good')
            {
            ?>
                <option value="Bad"  name='Bad'>Bad</option>
                <option selected value="Good"  name='Good'>Good</option>
            <?php
            }
            else if($customerDetails[0]->getStatus() == 'Bad')
            {
            ?>
                <option selected value="Bad"  name='Bad'>Bad</option>
                <option value="Good"  name='Good'>Good</option>
            <?php
            }
            ?>
            </select>       
        </div> 

        <input type="hidden" id="id" name="id" value="<?php echo $customerDetails[0]->getId() ?>">
        <input type="hidden" id="no_of_call" name="no_of_call" value="<?php echo $customerDetails[0]->getNoOfCall() ?>">
        <input type="hidden" id="customer_name" name="customer_name" value="<?php echo $customerDetails[0]->getName() ?>">
        <input type="hidden" id="customer_phoneno" name="customer_phoneno" value="<?php echo $customerDetails[0]->getPhone();?>">
        <input type="hidden" id="tele_username" name="tele_username" value="<?php echo $userDetails->getUsername();?>">
        <input type="hidden" id="tele_uid" name="tele_uid" value="<?php echo $userDetails->getUid();?>">
        <input type="hidden" id="previous_status" name="previous_status" value="<?php echo $customerDetails[0]->getStatus();?>">

        <!-- <input type="hidden" id="arr[]" name="arr[]"> -->

        <div class="input50-div">
            <p class="input-title-p">Status2</p>
            <select class="clean tele-input" onchange="random_function()" id="update_type" value="<?php echo $customerDetails[0]->getType();?>" name="update_type">
            <option value="">Please Select a Status</option>
            <?php 
            for ($cntPro=0; $cntPro <count($statusDetails) ; $cntPro++)
            {
            ?>
                <option value="<?php echo $statusDetails[$cntPro]->getStatus();?>"> 
                <?php echo $statusDetails[$cntPro]->getStatus(); ?>
                <!--take in display the options -->
                </option>
            <?php
            }
            ?>
            </select>
        </div>

        <div class="input50-div second-input50">
            <p class="input-title-p">Reason</p>
            <div id="arr[]" value="<?php echo $customerDetails[0]->getReason();?>" name="arr[]">
                <div id="Good" style="display: none">
                    <?php
                    for ($cntPro=0; $cntPro <count($reasonDetails) ; $cntPro++)
                    {
                    ?>
                        <?php
                        if ($reasonDetails[$cntPro]->getStatus() =="Good")
                        {
                        ?>
                            <input name="arr[]" type="checkbox" value="<?php echo $reasonDetails[$cntPro]->getReasonA() ?>"><?php echo $reasonDetails[$cntPro]->getReasonA() ?>    
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </div>

                <div id="Bad" style="display: none">
                    <?php
                    for ($cntPro=0; $cntPro <count($reasonDetails) ; $cntPro++)
                    {
                    ?>
                        <?php
                        if ($reasonDetails[$cntPro]->getStatus() =="Bad")
                        {
                        ?>
                            <input name="arr[]" type="checkbox" value="<?php echo $reasonDetails[$cntPro]->getReasonA() ?>"><?php echo $reasonDetails[$cntPro]->getReasonA() ?>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </div>

                <div id="Other" style="display: none">
                    <?php
                    for ($cntPro=0; $cntPro <count($reasonDetails) ; $cntPro++)
                    {
                    ?>
                        <?php
                        if ($reasonDetails[$cntPro]->getStatus() =="Other")
                        {
                        ?>
                            <input name="arr[]" type="checkbox" value="<?php echo $reasonDetails[$cntPro]->getReasonA() ?>"><?php echo $reasonDetails[$cntPro]->getReasonA() ?>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </div>

                <div id="No Take Call" style="display: none">
                <?php
                for ($cntPro=0; $cntPro <count($reasonDetails) ; $cntPro++)
                {
                ?>
                    <?php
                    if ($reasonDetails[$cntPro]->getStatus() =="No Take Call")
                    {
                    ?>
                        <input name="arr[]" type="checkbox" value="<?php echo $reasonDetails[$cntPro]->getReasonA() ?>"><?php echo $reasonDetails[$cntPro]->getReasonA() ?>
                    <?php
                    }
                    ?>
                <?php
                }
                ?>
                </div>
            </div>
        </div>

        <div class="clear"></div> 

        <div class="input50-div">
            <p class="input-title-p">Voice Recording</p>
            <input name="file" type="file" class="file-upload-field">
            <!-- <input name="file" type="file" class="file-upload-field" required> -->
            <!-- <div class="file-upload-wrapper" data-text="Select a MP3">
                <input name="file" type="file" class="file-upload-field" required>
            </div> -->
        </div>

        <div class="clear"></div> 

        <!-- <div class="width100"> -->
        <div class="input50-div">
            <p class="input-title-p">Remark</p>
            <input class="clean tele-input" type="text" value="<?php echo $customerDetails[0]->getRemark();?>" placeholder="Remark" id="update_remark" name="update_remark">
        </div>

        <!-- <div class="input50-div"> -->
        <div class="input50-div second-input50">
            <p class="input-title-p">Remark 2</p>
            <input class="clean tele-input" type="text" placeholder="Remark 2" id="update_remark_two" name="update_remark_two">    
        </div> 

        <!-- <div class="input50-div second-input50"> -->
        <div class="input50-div">
            <p class="input-title-p">Industrial Field</p>
            <input class="clean tele-input" type="text" placeholder="Industrial Field" id="update_occupation" name="update_occupation">    
        </div> 

        <!-- <div class="input50-div"> -->
        <div class="input50-div second-input50">
            <p class="input-title-p">Occupation</p>
            <input class="clean tele-input" type="text" placeholder="Occupation" id="update_hobby" name="update_hobby">    
        </div> 

        <!-- <div class="input50-div">
            <p class="input-title-p">Location</p>
            <input class="clean tele-input" type="text" placeholder="Location" id="update_location" name="update_location">    
        </div>  -->

    <?php
    }
    ?>

    <div class="clear"></div>

    <button class="clean red-btn margin-top30 fix300-btn" type="submit" id ="editSubmit" name ="editSubmit" ><b>Submit</b></a></button>

    </form>

</div>

<style>
.dashboard-li{
	color:#bf1b37;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

<script>
    function random_function()
    {
        var input = document.getElementById("update_type");

        if(update_type="Good")
        {
        var arr = document.getElementById("Good");
        arr.style.display = input.value == "Good" ? "block" : "none";
        }

        if(update_type="Bad")
        {
        var arr = document.getElementById("Bad");
        arr.style.display = input.value == "Bad" ? "block" : "none";
        }

        if(update_type="Other")
        {
        var arr = document.getElementById("Other");
        arr.style.display = input.value == "Other" ? "block" : "none";
        }

        else(update_type="No Take Call")
        {
        var arr = document.getElementById("No Take Call");
        arr.style.display = input.value == "No Take Call" ? "block" : "none";
        }
    }
</script>

</body>
</html>