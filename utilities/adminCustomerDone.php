<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $phone = $_POST["customer_phone"];

     $customerId = $_POST["customer_id"];

     $action = "Done";

     $filter = "Pending";

     $ctmDetails = getCustomerDetails($conn," WHERE phone = ? ",array("phone"),array($phone),"s");
     $type = $ctmDetails[0]->getType();

     // $action = "Reject";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $id."<br>";
     // echo $reasonName."<br>";


     $customerDetails = getCustomerDetails($conn," phone = ?  ",array("phone"),array($phone),"s");    

     if(!$customerDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($action)
          {
               array_push($tableName,"action");
               array_push($tableValue,$action);
               $stringType .=  "s";
          }

          if($filter)
          {
               array_push($tableName,"filter");
               array_push($tableValue,$filter);
               $stringType .=  "s";
          }

          // array_push($tableValue,$phone);
          // $stringType .=  "s";
          // $updatedReason = updateDynamicData($conn,"customerdetails"," WHERE phone = ? ",$tableName,$tableValue,$stringType);
          
          array_push($tableValue,$phone,$customerId);
          $stringType .=  "si";
          $updatedReason = updateDynamicData($conn,"customerdetails"," WHERE phone = ? AND id = ? ",$tableName,$tableValue,$stringType);
          if($updatedReason)
          {
               // echo "success";
               // echo "<script>alert('Update Action as Done');window.location='../checkLogGood.php'</script>";   

               echo "<script>alert('Updated !');window.location='../checkLogGood.php'</script>";   
               
               // original function
               // if($type == 'Good')
               // {
               //      // $_SESSION['messageType'] = 1;
               //      // header('Location: ../profile.php?type=1');
               //      echo "<script>alert('Updated !');window.location='../checkLogGood.php'</script>";    
               // }
               // else
               // {
               //      echo "<script>alert('Updated !');window.location='../checkLogUpdated.php'</script>"; 
               // }

          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to update action !!');window.location='../checkLogGood.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../checkLogGood.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>