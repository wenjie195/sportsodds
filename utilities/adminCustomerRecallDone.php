<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/SecondCustomerDetails.php';
// require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $phone = $_POST["customer_phone"];
     $customerId = $_POST["customer_id"];
     
     $action = "Done";
     $filter = "Pending";

     // $ctmDetails = getCustomerDetails($conn," WHERE phone = ? ",array("phone"),array($phone),"s");
     // $type = $ctmDetails[0]->getType();
     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $id."<br>";
     // echo $reasonName."<br>";

     $customerDetails = getSecCustomerDetails($conn," phone = ?  ",array("phone"),array($phone),"s");    

     if(!$customerDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($action)
          {
               array_push($tableName,"action");
               array_push($tableValue,$action);
               $stringType .=  "s";
          }
          if($filter)
          {
               array_push($tableName,"filter");
               array_push($tableValue,$filter);
               $stringType .=  "s";
          }

          // array_push($tableValue,$phone);
          // $stringType .=  "s";
          // $updatedReason = updateDynamicData($conn,"customerdetails"," WHERE phone = ? ",$tableName,$tableValue,$stringType);
          
          array_push($tableValue,$phone,$customerId);
          $stringType .=  "si";
          $updatedReason = updateDynamicData($conn,"second_customer_details"," WHERE phone = ? AND id = ? ",$tableName,$tableValue,$stringType);
          if($updatedReason)
          {
               echo "<script>alert('Updated !');window.location='../checkLogRecallGood.php'</script>";   
          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to update action !!');window.location='../checkLogRecallGood.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../checkLogRecallGood.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>