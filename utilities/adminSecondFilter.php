<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/SecondCustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $conn = connDB();

  $uid = $_SESSION['uid'];
  $Arr = $_POST["arr"];
  $newTele = $_POST['new_tele'];
  $ArrImplode = implode(",",$Arr); //display array value
  $ArrExplode = explode(",",$ArrImplode);

  $filter = 'Update';
}
else
{
   header('Location: ../index.php');
}


// function timeTeleUpdate($conn,$newTele,$customerName,$customerPhone,$updateStatus,$teleName,$updateType,$updateReason)
// {
//   if(insertDynamicData($conn,"second_customer_details",array("tele_name","name","phone","status","previous_tele","type","reason"),
//   array($newTele,$customerName,$customerPhone,$updateStatus,$teleName,$updateType,$updateReason),"sssssss") === null)
//   {
//     return false;
//   }
//   else
//   {}
//   return true;
// }

function timeTeleUpdate($conn,$newTele,$customerName,$customerPhone,$updateStatus,$teleName,$updateType,$updateReason)
{
  if(insertDynamicData($conn,"second_customer_details",array("tele_name","name","phone","previous_status","previous_tele","previous_type","previous_reason"),
  array($newTele,$customerName,$customerPhone,$updateStatus,$teleName,$updateType,$updateReason),"sssssss") === null)
  {
    return false;
  }
  else
  {}
  return true;
}

if ($ArrExplode)
{
  for ($i=0; $i <count($ArrExplode) ; $i++)
  {
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    if($filter)
    {
      array_push($tableName,"filter");
      array_push($tableValue,$filter);
      $stringType .=  "s";
    }
    array_push($tableValue,$ArrExplode[$i]);
    $stringType .=  "s";
    $customerDetails = updateDynamicData($conn,"customerdetails", "WHERE phone=?",$tableName,$tableValue,$stringType);
    $customerDetails = getCustomerDetails($conn, "WHERE phone=?",array("phone"),array($ArrExplode[$i]), "s");
    if ($customerDetails)
    {
      $teleName = $customerDetails[0]->getTeleName();
      $customerPhone = $customerDetails[0]->getPhone();
      $customerName = $customerDetails[0]->getName();
      $updateStatus = $customerDetails[0]->getStatus();
      $updateType = $customerDetails[0]->getType();
      $updateReason = $customerDetails[0]->getReason();

      if (timeTeleUpdate($conn,$newTele,$customerName,$customerPhone,$updateStatus,$teleName,$updateType,$updateReason))
      {
        $_SESSION['messageType'] = 1;
        header('location: ../checkLogUpdated2.php');
      }
      else
      {
        echo "fail !!";
      }
    }
  }
}
?>