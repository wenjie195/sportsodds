<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Notepad.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $noteUid = $_POST["notepad_uid"];
     $title = $_POST["update_title"];
     $content = $_POST["update_content"];

     $notesDetails = getNotepad($conn," uid = ? ",array("uid"),array($noteUid),"s");    

     if(!$notesDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($content)
          {
               array_push($tableName,"content");
               array_push($tableValue,$content);
               $stringType .=  "s";
          }

          array_push($tableValue,$noteUid);
          $stringType .=  "s";
          $updatedNotes = updateDynamicData($conn,"notepad"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updatedNotes)
          {
               // echo "success";
               echo "<script>alert('successfully updated notes details');window.location='../teleSpeechDraft.php'</script>";   
          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to update notes details');window.location='../teleSpeechDraft.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../teleSpeechDraft.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>