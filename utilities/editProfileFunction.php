<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $email = rewrite($_POST["update_email"]);
    $address = rewrite($_POST["update_address"]);
    $phone = rewrite($_POST["update_phone"]);

    //   FOR DEBUGGING 
    // echo "<br>";
    // echo $fullname."<br>";
    // echo $register_email."<br>";
    // echo $register_contact."<br>";

    $user = getUser($conn," uid = ?  ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../editProfile.php?type=1');
            echo "<script>alert('Profile Updated !');window.location='../teleEditProfile.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to update profile !!');window.location='../teleEditProfile.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../teleEditProfile.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
