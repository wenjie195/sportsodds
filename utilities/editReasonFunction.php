<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Reason.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $id = $_POST["reason_id"];
     $reasonName = $_POST["edit_reason_name"];

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $id."<br>";
     // echo $reasonName."<br>";


     $reasonDetails = getReason($conn," id = ?   ",array("id"),array($id),"i");    

     if(!$reasonDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($reasonName)
          {
               array_push($tableName,"reason");
               array_push($tableValue,$reasonName);
               $stringType .=  "s";
          }

          array_push($tableValue,$id);
          $stringType .=  "s";
          $updatedReason = updateDynamicData($conn,"reason"," WHERE id = ? ",$tableName,$tableValue,$stringType);
          if($updatedReason)
          {
               // echo "success";
               echo "<script>alert('successfully edit reason');window.location='../adminStatusReason.php'</script>";   
          }
          else
          {
               // echo "fail to update";
               echo "<script>alert('fail to edit reason');window.location='../adminStatusReason.php'</script>";   
          }
     }
     else
     {
          // echo "GG";
          echo "<script>alert('ERROR !!');window.location='../adminStatusReason.php'</script>";   
     }
}
else 
{
     header('Location: ../index.php');
}
?>