<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$updateTime = $dt->format('Y-m-d H:i:s');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = $_POST["id"];
    $update_status = $_POST["update_status"];
    $update_remark = $_POST["update_remark"];

    $no_of_call = $_POST["no_of_call"];
    $update_no_of_call = $no_of_call + 1;

    $tele_username = $_POST["tele_username"];
    $tele_uid = $_POST["tele_uid"];
    $customer_name = $_POST["customer_name"];

    $customer_phoneno = $_POST["customer_phoneno"];

    //additional task
    $update_name = $_POST["update_name"];
    $update_type = $_POST["update_type"];
    // $update_reason = $_POST["update_reason"];
    $Arr = $_POST["arr"];
    $ArrImplode = implode(", ",$Arr); //display array value
    //$ArrImplode = implode($Arr); //display array value
    // $ArrImplode = implode($Arr); //display array value

    $comp = $_POST["update_company"];
    $remarkB = $_POST["update_remark_two"];
    $occupation = $_POST["update_occupation"];
    $hobby = $_POST["update_hobby"];
    // $location = $_POST["update_location"];
    $location = " - ";

    $recording = $_FILES['file']['name'];

    $target_dir = "../upload_recording/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Valid file extensions
    // $extensions_arr = array("jpg","jpeg","png","gif");
    // $extensions_arr = array("jpg","jpeg","png","gif","mp3","m4a");
    $extensions_arr = array("jpg","jpeg","png","gif","mp3","m4a","wma");

    if( in_array($imageFileType,$extensions_arr) )
    {
    move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$recording);
    }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $id."<br>";
    // echo $update_status."<br>";
    // echo $update_remark."<br>";
    // echo $no_of_call."<br>";
    // echo $update_no_of_call."<br>";
    // echo $tele_username."<br>";
    // echo $tele_uid."<br>";
    // echo $customer_name."<br>";
    // echo $update_name."<br>";
    // echo $update_type."<br>";
    // // echo $update_reason."<br>";
    // // echo $Arr."<br>";
    // echo $ArrImplode."<br>";

}

if(isset($_POST['editSubmit']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($update_status)
    {
        array_push($tableName,"status");
        array_push($tableValue,$update_status);
        $stringType .=  "s";
    }
    if($update_remark)
    {
        array_push($tableName,"remark");
        array_push($tableValue,$update_remark);
        $stringType .=  "s";
    }
    if($update_no_of_call)
    {
        array_push($tableName,"no_of_call");
        array_push($tableValue,$update_no_of_call);
        $stringType .=  "s";
    }

    if($update_name)
    {
        array_push($tableName,"name");
        array_push($tableValue,$update_name);
        $stringType .=  "s";
    }
    if($update_type)
    {
        array_push($tableName,"type");
        array_push($tableValue,$update_type);
        $stringType .=  "s";
    }
    // if($update_reason)
    // {
    //     array_push($tableName,"reason");
    //     array_push($tableValue,$update_reason);
    //     $stringType .=  "s";
    // }
    if($ArrImplode)
    {
        array_push($tableName,"reason");
        array_push($tableValue,$ArrImplode);
        $stringType .=  "s";
    }
    if($comp)
    {
        array_push($tableName,"company_name");
        array_push($tableValue,$comp);
        $stringType .=  "s";
    }
    if($recording)
    {
        array_push($tableName,"recording");
        array_push($tableValue,$recording);
        $stringType .=  "s";
    } 
    if($remarkB)
    {
        array_push($tableName,"remark_two");
        array_push($tableValue,$remarkB);
        $stringType .=  "s";
    } 
    if($occupation)
    {
        array_push($tableName,"occupation");
        array_push($tableValue,$occupation);
        $stringType .=  "s";
    } 
    if($hobby)
    {
        array_push($tableName,"hobby");
        array_push($tableValue,$hobby);
        $stringType .=  "s";
    } 
    if($location)
    {
        array_push($tableName,"location");
        array_push($tableValue,$location);
        $stringType .=  "s";
    } 

    if($updateTime)
    {
        array_push($tableName,"last_updated");
        array_push($tableValue,$updateTime);
        $stringType .=  "s";
    } 

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateCustomerDetails = updateDynamicData($conn,"customerdetails"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($updateCustomerDetails)
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../teleDashboard.php');

        $uid = $tele_uid;
        $teleName = $tele_username;
        $customerName = $update_name;
        $updateStatus = $update_status;
        $updateRemark = $update_remark;
        $noOfuUpdate = $update_no_of_call;

        $customerPhone = $customer_phoneno;

        $type = $update_type;
        $reason = $ArrImplode;
        $companyName = $comp;

        // if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording))
        if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location))
        {
            // echo "to other side";
            echo "<script>alert('Data Updated and Stored !');window.location='../teleDashboard.php'</script>";   
        }
        else
        {
            // echo "fail to 2nd side";
            echo "<script>alert('Error Level 2');window.location='../teleDashboard.php'</script>";   
        }

        // echo "done part 1";   

    }
    else
    {
        echo "<script>alert('Fail to Update Data !');window.location='../teleDashboard.php'</script>";     
        // echo "fail";
    }
}
else
{
   //echo "dunno";
   header('Location: ../index.php');
}


function timeTeleUpdate($conn,$uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location)
{
     if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","customer_phone","update_status","update_remark","no_of_update","type","reason","company_name","recording","remark_two","occupation","hobby","location"),
     array($uid,$teleName,$customerName,$customerPhone,$updateStatus,$updateRemark,$noOfuUpdate,$type,$reason,$companyName,$recording,$remarkB,$occupation,$hobby,$location),"sssssssssssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
?>