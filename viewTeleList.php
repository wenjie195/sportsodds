<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE user_type =1 ");
$userRows = getUser($conn," WHERE user_type !=0 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Telemarketer | adminTele" />
    <title>Telemarketer | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <!-- <h1 class="h1-title">All Telemarketer List</h1> -->
    <h1 class="h1-title">Staff List</h1>
    
    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <!-- <th>MEMBER ID</th> -->
                            <th>NAME</th>
                            <!-- <th>FULLNAME</th> -->
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            <th>JOINED DATE</th>
                            <th>KPI</th>
                            <th>EDIT PROFILE</th>
                            <th>EDIT PASSWORD</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($userRows)
                        {   
                            for($cnt = 0;$cnt < count($userRows) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td><?php //echo $userRows[$cnt]->getId();?></td> -->
                                <!-- <td><?php //echo $userRows[$cnt]->getUid();?></td> -->
                                <!-- <td><?php //echo $userRows[$cnt]->getUsername();?></td> -->
                                <td><?php echo $userRows[$cnt]->getFullName();?></td>
                                <td><?php echo $userRows[$cnt]->getEmail();?></td>
                                <td><?php echo $userRows[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userRows[$cnt]->getAddress();?></td>
                                
                                <td>
                                    <?php echo $date = date("d-m-Y",strtotime($userRows[$cnt]->getDateCreated()));?>
                                </td>

                                <td>
                                    <form action="teleKPIList.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="tele_username" value="<?php echo $userRows[$cnt]->getUsername();?>">
                                            <img src="img/kpi.png" class="width100 hover1a" alt="KPI" title="KPI">
                                            <img src="img/kpi2.png" class="width100 hover1b" alt="KPI" title="KPI">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="adminEditTeleProfile.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="tele_uid" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="adminEditTelePass.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="tele_uid" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>
</div>
<style>
.telemarketer-li{
	color:#bf1b37;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>